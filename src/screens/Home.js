import React, { useEffect, useRef, useState } from 'react'
import { StyleSheet, Text, View, DrawerLayoutAndroid, Button, ScrollView } from 'react-native'

import { globalStyles, spacing } from '../utils/styles'
import { hp, wp } from '../utils/dimension'
import { colors } from '../utils/colors'

import Layout from '../components/Layout'
import Header from '../components/Header'
import ProductCard from '../components/ProductCard'
import DropDown from '../components/DropDown'
import Order from '../components/Order'
import OrderConfirmation from '../components/OrderConfirmation'

// Dummy data
const menus = [1, 2, 3, 4, 5, 6];

const product = {
  img: require('../assets/images/pasta.png'),
  title: 'Spicy seasoned seafood noodles',
  price: 10000,
  stock: 20
};

const Home = ({ route }) => {
  const { setActiveDrawer } = route.params;
  const drawer = useRef(null);

  const [drawerPosition, setDrawerPosition] = useState("right");

  const orderDrawer = () => {
    return (
      <View style={{ flex: 1, backgroundColor: 'blue', justifyContent: 'center', alignItems: 'center' }}>
        <Button
          title="Close drawer"
          onPress={() => {
            setActiveDrawer(false)
            drawer.current.closeDrawer()
          }}
        />
      </View>
    )
  };

  return (
    <Layout>
      <DrawerLayoutAndroid
        ref={drawer}
        drawerWidth={500}
        drawerPosition={drawerPosition}
        renderNavigationView={OrderConfirmation}
        onDrawerClose={() => setActiveDrawer(false)}
      >
        <View style={styles.mainContent}>
          <View style={styles.middleContent}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Header title='Home' type='home' />
              {/* Top Nav */}
              <View style={styles.menuSection}>
                <Text style={globalStyles.titleMed}>Choose Products</Text>
                <DropDown />
              </View>
              <View style={styles.menuList}>
                {
                  menus.map((item, index) => (
                    <ProductCard key={index} product={product} />
                  ))
                }
              </View>
            </ScrollView>
          </View>

          <View style={styles.order}>
            <Order onConfirmPayment={() => {
                setActiveDrawer(true)
                drawer.current.openDrawer()
              }}
            />
          </View>
        </View>
      </DrawerLayoutAndroid>
    </Layout>
  )
}

export default Home

const styles = StyleSheet.create({
  mainContent: {
    flexDirection: 'row'
  },
  middleContent: {
    padding: wp(1.5), 
    width: wp(63)
  },
  menuSection: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: spacing.medium,
  },
  menuList: { 
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    flexWrap: 'wrap',
  },
  order: {
    flex: 1,
    backgroundColor: colors.darkBg,
    padding: wp(1.5),
  }
})