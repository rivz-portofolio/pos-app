import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import Layout from '../components/Layout'
import Header from '../components/Header'
import { wp } from '../utils/dimension'

const Settings = () => {
  return (
    <Layout>
      <View style={{ padding: wp(1.5) }}>
        <Header title='Settings' />
        <Text>Settings</Text>
      </View>
    </Layout>
  )
}

export default Settings

const styles = StyleSheet.create({})