import React, { useState } from 'react';
import { Image, Pressable, StyleSheet, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';

import { hp, wp } from '../utils/dimension';
import { colors } from '../utils/colors';
import { spacing } from '../utils/styles';

import logoResto from '../assets/images/logo-resto.png'

import Home from '../screens/Home';
import Settings from '../screens/Settings';

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
  const [activeDrawer, setActiveDrawer] = useState(false);

  return (
    <Tab.Navigator
        screenOptions={{
            headerShown: false,
        }}
        tabBar={({ navigation, state }) => {
            return (
                <View style={styles.sidebar}>
                    <View style={styles.logoWrapper}>
                        <Image source={logoResto} style={styles.logo} />
                    </View>
                    <Pressable
                        style={[styles.menuBtn, { backgroundColor: state.index === 0 ? colors.mainBg : colors.darkBg }]}
                        onPress={() => activeDrawer ? null : navigation.navigate('Home')}
                    >
                        <Icon
                            name="home-outline"
                            size={wp(2)}
                            color={state.index === 0 ? colors.white : colors.orange}
                            style={{ backgroundColor: state.index === 0 ? colors.orange : colors.darkBg, padding: 10, borderRadius: 8 }}
                        />
                    </Pressable>
                    <Pressable
                        style={[styles.menuBtn, { backgroundColor: state.index === 1 ? colors.mainBg : colors.darkBg }]}
                        onPress={() => activeDrawer ? null : navigation.navigate('Settings')}
                    >
                        <Icon 
                            name="settings-outline"
                            size={wp(2)}
                            color={state.index === 1 ? colors.white : colors.orange}
                            style={{ backgroundColor: state.index === 1 ? colors.orange : colors.darkBg, padding: 10, borderRadius: 8 }}
                        />
                    </Pressable>
                </View>
            )
        }}
    >
      <Tab.Screen name="Home" component={Home} initialParams={{setActiveDrawer}} />
      <Tab.Screen name="Settings" component={Settings} />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  sidebar: { 
    position: 'absolute',
    height: hp(100),
    width: wp(6),
    zIndex: 10,
    backgroundColor: colors.darkBg,
    alignItems: 'center',
    overflow: 'hidden'
  },
  logoWrapper: {
    height: '10%', 
    width: '100%', 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  logo: {
    width: wp(4.5),
    height: wp(4.5)
  },
  menuBtn: {
    padding: spacing.small,
    width: '100%',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    marginLeft: spacing.small,
    marginBottom: spacing.small
  }
})

export default MainNavigator;