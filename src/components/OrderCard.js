import React from 'react'
import { Image, StyleSheet, Text, View, TextInput } from 'react-native'

import pasta from '../assets/images/pasta.png'

import { colors } from '../utils/colors'
import { wp } from '../utils/dimension'
import { globalStyles, spacing } from '../utils/styles'
import ButtonIcon from './ButtonIcon'

const OrderCard = ({ order }) => {
    return (
        <View style={styles.container}>
            <View style={styles.leftOrder}>
                <View style={styles.leftOrderUpper}>
                    <Image source={order.img} style={styles.img} />
                    <View style={{ flex: 1, marginHorizontal: wp(1) }}>
                        <Text style={globalStyles.titleSmall} numberOfLines={1}>{order.title}</Text>
                        <Text style={[globalStyles.text, { color: colors.grayText }]}>Rp {order.price}</Text>
                    </View>
                    <Text style={[globalStyles.titleSmall, styles.qty]}>{order.qty}</Text>
                    <View style={styles.rightOrder}>
                        <Text style={[globalStyles.titleXSmall]}>Rp. 20k</Text>
                    </View>
                </View>
                <View style={styles.leftOrderBottom}>
                    <TextInput
                        placeholder='Additional Notes...'
                        placeholderTextColor={colors.placeholderText}
                        style={[globalStyles.text, styles.txtInput]}
                    />
                    <View style={styles.rightOrder}>
                        <ButtonIcon />
                    </View>
                </View>
            </View>
        </View>
    )
}

export default OrderCard

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginBottom: spacing.medium
    },
    leftOrder: {
        flex: 1,
    },
    img: {
        width: wp(4),
        height: wp(4),
    },
    leftOrderUpper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    leftOrderBottom: {
        flexDirection: 'row',
        marginTop: spacing.small,
        justifyContent: 'space-between'
    },
    txtInput: {
        flex: 1,
        backgroundColor: colors.lightBg,
        paddingVertical: spacing.xsmall,
        paddingHorizontal: spacing.small,
        borderRadius: 8,
    },
    qty: {
        backgroundColor: colors.lightBg,
        paddingHorizontal: wp(1.2),
        paddingVertical: wp(1),
        borderRadius: 8

    },
    rightOrder: {
        width: '20%',
        alignItems: 'flex-end',
    },
})