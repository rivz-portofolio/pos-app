import React from 'react'
import { Pressable, StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';

import { wp } from '../utils/dimension'
import { colors } from '../utils/colors'

const ButtonIcon = ({ onPress }) => {
    return (
        <Pressable
            style={styles.btn}
            onPress={onPress}
        >
            <Icon name="trash-outline" size={wp(1.5)} color={colors.redAccent} />
        </Pressable>
    )
}

export default ButtonIcon

const styles = StyleSheet.create({
    btn: {
        paddingHorizontal: wp(1),
        paddingVertical: wp(0.9),
        alignItems: 'center',
        borderRadius: 8,
        borderWidth: 1,
        borderColor: colors.redAccent,
    },
})