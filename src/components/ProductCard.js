import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

import { globalStyles, spacing } from '../utils/styles'
import { colors } from '../utils/colors'
import { wp } from '../utils/dimension'

const ProductCard = ({ product }) => {
  return (
    <View style={styles.productCard}>
      <Image source={product.img} style={styles.img} />
      <Text style={[globalStyles.titleSmall, { textAlign: 'center' }]}>{product.title}</Text>
      <Text style={[globalStyles.text, { marginVertical: 10 }]}>Rp. {product.price}</Text>
      <Text style={[globalStyles.text, { color: colors.grayText }]}>{product.stock} Bowls available</Text>
    </View>
  )
}

export default ProductCard

const styles = StyleSheet.create({
  productCard: {
    width: '30%',
    backgroundColor: colors.darkBg,
    padding: spacing.medium,
    marginBottom: spacing.medium,
    borderRadius: 16,
    alignItems: 'center'
  },
  img: {
    width: '30%',
    aspectRatio: 1 / 1,
    marginBottom: 10,
  }
})