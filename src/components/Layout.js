import React from 'react'
import { StyleSheet, View } from 'react-native'

import { wp } from '../utils/dimension'
import { colors } from '../utils/colors'

const Layout = ({ children }) => {
  return (
    <View style={styles.container}>
          <View style={styles.sidebar} />
          <View style={styles.content}>
            { children }
          </View>
    </View>
  )
}

export default Layout

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      backgroundColor: colors.mainBg,
    },
    sidebar: { 
      height: '100%',
      width: wp(6)
    },
    content: { 
      flex: 1, 
    }
})