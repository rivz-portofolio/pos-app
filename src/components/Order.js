import React, { useState } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'

import { globalStyles, spacing } from '../utils/styles'
import { hp, wp } from '../utils/dimension'
import { colors } from '../utils/colors'

import Chip from './Chip'
import Button from './Button'
import OrderCard from './OrderCard'

const Order = ({ onConfirmPayment }) => {
    const [activeChip, setActiveChip] = useState(0);
    const orderMenu = Array.from(Array(5).keys())

    const chipMenu = [
        {
            title: 'Dine In'
        },
        {
            title: 'To Go'
        },
        {
            title: 'Delivery'
        },
    ];

    const order = {
        img: require('../assets/images/pasta.png'),
        title: 'Spicy seasoned seafood noodles',
        price: 10000,
        qty: 2
    };

    return (
        <View  style={{ flex: 1 }}>
            <Text style={globalStyles.titleMed}>Orders #34562</Text>
            <View style={styles.chipWrapper}>
                { chipMenu.map((item, index) => (
                    <Chip key={index} title={item.title} active={activeChip === index} onPress={() => setActiveChip(index)} />
                ))}
            </View>
            <View style={styles.orderMenuWrapper}>
                <Text style={globalStyles.titleSmall}>Item</Text>
                <View style={styles.orderMenuInner}>
                    <Text style={globalStyles.titleSmall}>Qty</Text>
                    <Text style={globalStyles.titleSmall}>Price</Text>
                </View>
            </View>
            <View style={styles.orderMenu}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    { orderMenu.map((item, index) => (
                        <OrderCard key={index} order={order} />
                    ))}
                </ScrollView>
            </View>
            <View style={styles.orderMenuFooter}>
                <View style={styles.orderMenuFooterList}>
                    <Text style={[globalStyles.text, { color: colors.grayText }]}>Discount</Text>
                    <Text style={globalStyles.text}>Rp 0</Text>
                </View>
                <View style={styles.orderMenuFooterList}>
                    <Text style={[globalStyles.text, { color: colors.grayText }]}>Sub Total</Text>
                    <Text style={globalStyles.text}>Rp 100.000</Text>
                </View>
                <Button title='Confirm Payment' active onPress={onConfirmPayment} />
            </View>
        </View>
    )
}

export default Order

const styles = StyleSheet.create({
    chipWrapper: {
        flexDirection: 'row',
        marginVertical: hp(2)
    },
    orderMenuWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: colors.borderDark,
        paddingBottom: spacing.small
    },
    orderMenu: {
        flex: 1,
        paddingTop: wp(1.5),
        borderBottomWidth: 2,
        borderColor: colors.borderDark
    },
    orderMenuInner: {
        width: '30%',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    orderMenuFooter: {
        marginTop: hp(2)
    },
    orderMenuFooterList: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: hp(2)
    }
})