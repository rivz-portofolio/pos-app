import React from 'react'
import { Pressable, StyleSheet, Text } from 'react-native'

import { wp } from '../utils/dimension'
import { colors } from '../utils/colors'
import { globalStyles } from '../utils/styles'

const Chip = ({ title, onPress, active }) => {
    return (
        <Pressable
            style={[styles.chip, !active && styles.inactiveChip]}
            onPress={onPress}
        >
        <Text style={[globalStyles.text, !active && styles.inactiveTextChip]}>{title}</Text>
        </Pressable>
    )
}

export default Chip

const styles = StyleSheet.create({
    chip: {
        paddingHorizontal: wp(1),
        paddingVertical: wp(0.6),
        backgroundColor: colors.orange,
        borderRadius: 8,
        marginRight: wp(1),
        borderWidth: 1,
    },
    inactiveChip: {
        backgroundColor: colors.darkBg,
        borderWidth: 1,
        borderColor: colors.borderDark
    },
    inactiveTextChip: {
        color: colors.orange
    }
})