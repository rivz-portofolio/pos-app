import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';

import { colors } from '../utils/colors'
import { globalStyles, spacing } from '../utils/styles'
import { wp } from '../utils/dimension';

const Search = () => {
    return (
        <View style={styles.search}>
            <Icon name="search-outline" size={wp(2)} color="#FFF" />
            <TextInput
                placeholder='Search for products...'
                placeholderTextColor={colors.placeholderText}
                style={[globalStyles.text, styles.txtInput]}
            />
        </View>
    )
}

export default Search

const styles = StyleSheet.create({
    search: {
        backgroundColor: colors.lightBg,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: spacing.small,
        borderRadius: 8,
        width: wp(20)
    },
    txtInput: {
        flex: 1,
        paddingLeft: spacing.small,
    }
})