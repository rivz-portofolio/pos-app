import React from 'react'
import { Pressable, StyleSheet, Text } from 'react-native'

import { globalStyles } from '../utils/styles'
import { wp } from '../utils/dimension'
import { colors } from '../utils/colors'

const Button = ({ title, active, onPress }) => {
  return (
    <Pressable
        style={[styles.btn, !active && styles.inactiveBtn]}
        onPress={onPress}
    >
        <Text style={[globalStyles.titleSmall, !active && styles.inactiveTextBtn]}>{title}</Text>
    </Pressable>
  )
}

export default Button

const styles = StyleSheet.create({
    btn: {
        paddingHorizontal: wp(1),
        paddingVertical: wp(0.9),
        backgroundColor: colors.orange,
        borderRadius: 8,
        borderWidth: 1,
        alignItems: 'center'
    },
    inactiveBtn: {
        backgroundColor: colors.darkBg,
        borderWidth: 1,
        borderColor: colors.borderDark
    },
    inactiveTextBtn: {
        color: colors.orange
    }
})