import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'

import { globalStyles } from '../utils/styles'
import Search from './Search'

const Header = ({ title, type = '' }) => {
  return (
    <View style={styles.header}>
        <View>
            <Text style={globalStyles.title}>{title}</Text>
            <Text style={globalStyles.text}>Tuesday, 2 Feb 2022</Text>
        </View>
        { type === 'home' &&
            <Search />
        }
    </View>
  )
}

export default Header

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})