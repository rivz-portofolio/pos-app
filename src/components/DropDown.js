import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Picker } from '@react-native-picker/picker';

import { wp } from '../utils/dimension';
import { colors } from '../utils/colors';
import { spacing } from '../utils/styles';

const DropDown = () => {
  const [selectedItem, setSelectedItem] = useState();

  return (
    <View style={{ width: wp(15), borderRadius: 8, overflow: 'hidden' }}>
      <Picker
        selectedValue={selectedItem}
        style={{ backgroundColor: colors.darkBg, color: colors.white }}
        dropdownIconColor={colors.white}
        onValueChange={(itemValue, itemIndex) =>
          setSelectedItem(itemValue)
        }>
        <Picker.Item label="Dine In" value="di" />
        <Picker.Item label="Take Away" value="ta" />
      </Picker>
    </View>
  )
}

export default DropDown

const styles = StyleSheet.create({})