import { Dimensions } from "react-native";

const widthPercentage = (percentage) => {
    return Dimensions.get('screen').width * ( percentage / 100 );
};

const heightPercentage = (percentage) => {
    return Dimensions.get('screen').height * ( percentage / 100 );
};

export {
    widthPercentage as wp,
    heightPercentage as hp,
}