export const colors = {
    mainBg: '#393C49',
    darkBg: '#1F1D2B',
    lightBg: '#2D303E',

    borderDark: '#393C49',

    placeholderText: '#ABBBC2',
    grayText: '#ABBBC2',

    orange: '#EA7C69',
    white: '#FFF',
    gray: 'gray',
    redAccent: '#FF7CA3',
}