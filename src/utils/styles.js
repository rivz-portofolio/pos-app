import { colors } from "./colors";
import { wp } from "./dimension";

export const globalStyles = {
    // Title
    title: {
        color: colors.white,
        fontSize: wp(2),
        fontWeight: 'bold',
    },
    titleMed: {
        color: colors.white,
        fontSize: wp(1.5),
        fontWeight: 'bold',
    },
    titleSmall: {
        color: colors.white,
        fontSize: wp(1.2),
        fontWeight: 'bold',
    },
    titleXSmall: {
        color: colors.white,
        fontSize: wp(1),
        fontWeight: 'bold',
    },
    // Text
    text: {
        color: colors.white,
        fontSize: wp(1),
    }
}

export const spacing = {
    large: wp(3),
    medium: wp(2),
    small: wp(1),
    xsmall: wp(0.5)
}